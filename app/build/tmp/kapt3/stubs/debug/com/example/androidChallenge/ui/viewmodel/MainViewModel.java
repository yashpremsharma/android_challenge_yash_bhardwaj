package com.example.androidChallenge.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\n\u001a\u00020\u000bJ\u0006\u0010\f\u001a\u00020\u000bR \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\r"}, d2 = {"Lcom/example/androidChallenge/ui/viewmodel/MainViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "notifyButtonClick", "Landroidx/lifecycle/MutableLiveData;", "", "getNotifyButtonClick", "()Landroidx/lifecycle/MutableLiveData;", "setNotifyButtonClick", "(Landroidx/lifecycle/MutableLiveData;)V", "flightSearchClick", "", "referFriends", "app_debug"})
public final class MainViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> notifyButtonClick;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getNotifyButtonClick() {
        return null;
    }
    
    public final void setNotifyButtonClick(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    public final void referFriends() {
    }
    
    public final void flightSearchClick() {
    }
    
    public MainViewModel() {
        super();
    }
}