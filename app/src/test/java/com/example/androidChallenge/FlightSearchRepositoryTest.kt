package com.example.androidChallenge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidChallenge.data.network.MyApi
import com.example.androidChallenge.data.network.SafeApiRequest
import com.example.androidChallenge.data.network.response.FlightSearchResult
import com.example.androidChallenge.data.repository.FlightSearchRepository
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class FlightSearchRepositoryTest {

    @Mock
    private lateinit var myAPi: MyApi

    @Mock
    private lateinit var safeApiRequest: SafeApiRequest

    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var flightSearchRepository: FlightSearchRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        flightSearchRepository = FlightSearchRepository(myAPi)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }
    @Test
    fun `main repo executes api call`(){
        testScope.runBlockingTest {

            //Given
            val flightSearchResult = Mockito.mock(FlightSearchResult::class.java)
            whenever( safeApiRequest.apiRequest { myAPi.getFlightList()}).thenReturn(flightSearchResult)

            //When
            myAPi.getFlightList()

            //Then
            verify(myAPi, times(1)).getFlightList()
            verifyNoMoreInteractions(myAPi)
        }
    }

}