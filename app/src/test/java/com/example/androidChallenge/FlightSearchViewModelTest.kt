package com.example.androidChallenge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidChallenge.data.network.response.FlightSearchResult
import com.example.androidChallenge.data.repository.FlightSearchRepository
import com.example.androidChallenge.ui.viewmodel.FlightSearchViewModel
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class FlightSearchViewModelTest {

    // Mock the repository object and verify interactions on this mock
    @Mock
    private lateinit var flightSearchRepository: FlightSearchRepository

    @Mock
    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var flightSearchViewModel: FlightSearchViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        flightSearchViewModel = FlightSearchViewModel(flightSearchRepository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun `main view model executes apiCall`() {

        testScope.runBlockingTest {

            val flightSearchResult = Mockito.mock(FlightSearchResult::class.java)
            whenever(flightSearchRepository.getFlightSearchResult()).thenReturn(flightSearchResult)

            verify(flightSearchRepository, times(0)).getFlightSearchResult()
            verifyNoMoreInteractions(flightSearchRepository)
        }
    }
}