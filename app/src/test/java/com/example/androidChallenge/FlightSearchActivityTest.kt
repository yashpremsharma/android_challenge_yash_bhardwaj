package com.example.androidChallenge

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidChallenge.data.network.response.FlightData
import com.example.androidChallenge.ui.activity.FlightSearchActivity
import com.example.androidChallenge.ui.activity.FlightSearchActivity.Companion.CHEAPEST
import com.example.androidChallenge.ui.activity.FlightSearchActivity.Companion.EARLIEST
import com.example.androidChallenge.ui.activity.FlightSearchActivity.Companion.FASTEST
import com.example.androidChallenge.ui.viewmodel.FlightSearchViewModel
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class FlightSearchActivityTest {
    @Mock
    private lateinit var flightSearchViewModel: FlightSearchViewModel

    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var flightSearchActivity: FlightSearchActivity

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        flightSearchActivity = FlightSearchActivity()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun `main activity executes api call`() {
        Mockito.doNothing().`when`(flightSearchViewModel).getFlightSearchResult()

        verify(flightSearchViewModel, times(0)).getFlightSearchResult()
        verifyNoMoreInteractions(flightSearchViewModel)
    }

    @Test
    fun `main activity executes sort by cheapest`() {
        val popularRepositoryData = Mockito.mock(FlightData::class.java)
        val repoList = listOf(popularRepositoryData)
        Mockito.doNothing().`when`(flightSearchViewModel).sortList(ArrayList(repoList), CHEAPEST)

        verify(flightSearchViewModel, times(0)).sortList(ArrayList(repoList), CHEAPEST)
        verifyNoMoreInteractions(flightSearchViewModel)
    }

    @Test
    fun `main activity executes sort by earliest`() {
        val popularRepositoryData = Mockito.mock(FlightData::class.java)
        val repoList = listOf(popularRepositoryData)
        Mockito.doNothing().`when`(flightSearchViewModel).sortList(ArrayList(repoList), EARLIEST)

        verify(flightSearchViewModel, times(0)).sortList(ArrayList(repoList), EARLIEST)
        verifyNoMoreInteractions(flightSearchViewModel)
    }

    @Test
    fun `main activity executes sort by fastest`() {
        val popularRepositoryData = Mockito.mock(FlightData::class.java)
        val repoList = listOf(popularRepositoryData)
        Mockito.doNothing().`when`(flightSearchViewModel).sortList(ArrayList(repoList), FASTEST)

        verify(flightSearchViewModel, times(0)).sortList(ArrayList(repoList), FASTEST)
        verifyNoMoreInteractions(flightSearchViewModel)
    }
}