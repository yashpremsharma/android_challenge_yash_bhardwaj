package com.example.androidChallenge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidChallenge.ui.activity.MainActivity.Companion.REFER
import com.example.androidChallenge.ui.activity.MainActivity.Companion.SEARCH

class MainViewModel() : ViewModel() {

    var notifyButtonClick = MutableLiveData<String>()

    fun referFriends() {
        notifyButtonClick.postValue(REFER)
    }

    fun flightSearchClick() {
        notifyButtonClick.postValue(SEARCH)
    }

}