package com.example.androidChallenge.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.DataBindingUtil.inflate
import androidx.recyclerview.widget.RecyclerView
import com.example.androidChallenge.R
import com.example.androidChallenge.data.network.response.Fares
import com.example.androidChallenge.databinding.LayoutItemProviderBinding
import com.example.androidChallenge.ui.viewmodel.FlightSearchViewModel
import com.example.androidChallenge.utils.*


class ProviderListAdapter(
    private val context: Context,
    var providerList: ArrayList<Fares>,
    private val model: FlightSearchViewModel
) : RecyclerView.Adapter<ProviderListAdapter.ProviderListViewHolder>() {

    private lateinit var layoutInflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProviderListViewHolder {

        layoutInflater = LayoutInflater.from(parent.context)
        val binding: LayoutItemProviderBinding =
            inflate(layoutInflater, R.layout.layout_item_provider, parent, false)
        return ProviderListViewHolder(binding.root, binding)
    }

    override fun getItemCount() = providerList.size

    override fun onBindViewHolder(holder: ProviderListViewHolder, position: Int) {
        val fareData: Fares = providerList[position]
        holder.binding.data = fareData
        holder.binding.viewModel = model

            when (fareData.providerId) {
                Providers.MAKEMYTRIP.value -> {
                    holder.providerName.text = "MakeMyTrip "
                }
                Providers.CLEARTRIP.value -> {
                    holder.providerName.text = "Cleartrip "
                }
                Providers.YATRA.value -> {
                    holder.providerName.text = "Yatra "
                }
                Providers.MUSAFIR.value -> {
                    holder.providerName.text = "Musafir "
                }
            }
    }

    class ProviderListViewHolder(itemView: View, var binding: LayoutItemProviderBinding) :
        RecyclerView.ViewHolder(itemView) {
        var providerName: AppCompatTextView = itemView.findViewById(R.id.providerName)
    }
}