package com.example.androidChallenge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidChallenge.data.network.response.FlightData
import com.example.androidChallenge.data.repository.FlightSearchRepository
import com.example.androidChallenge.ui.activity.FlightSearchActivity
import com.example.androidChallenge.ui.activity.FlightSearchActivity.Companion.APPLY
import com.example.androidChallenge.ui.activity.FlightSearchActivity.Companion.CLOSE
import com.example.androidChallenge.ui.activity.FlightSearchActivity.Companion.START
import com.example.androidChallenge.ui.adapter.FlightListAdapter
import com.example.androidChallenge.utils.ApiException
import com.example.androidChallenge.utils.NoInternetException
import kotlinx.android.synthetic.main.activity_flight_seach.*
import kotlinx.coroutines.launch
import java.text.ParseException
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class FlightSearchViewModel(
    private val repository: FlightSearchRepository
) : ViewModel() {

    val onStart = MutableLiveData<Any>()
    val onSuccess = MutableLiveData<Any>()
    val onFailure = MutableLiveData<String>()
    val closeDialogData = MutableLiveData<String>()
    val onSort = MutableLiveData<ArrayList<FlightData>>()

    fun getFlightSearchResult(){
        onStart.postValue(START)
        viewModelScope.launch {
            try {
                val response = repository.getFlightSearchResult()
                response.let {
                    val result = it.flights
                    onSuccess.postValue(result)
                    return@launch
                }
            } catch (ex: ApiException) {
                ex.printStackTrace()
                onFailure.postValue(ex.message.toString())
            } catch (ex: NoInternetException) {
                ex.printStackTrace()
                onFailure.postValue(ex.message.toString())
            }
        }
    }

    fun closeDialog() {
        closeDialogData.postValue(CLOSE)
    }

    fun onApplyClick() {
        closeDialogData.postValue(APPLY)
    }

    fun sortList(flightListArr: ArrayList<FlightData>, sortBy: String) {
        when (sortBy) {
            FlightSearchActivity.CHEAPEST -> {
                Collections.sort(
                    flightListArr,
                    Comparator<FlightData?> { flight1: FlightData?, flight2: FlightData? ->
                        var previousPrice = 0L
                        var nextPrice = 0L
                        try {
                            flight1?.lowestPrice?.let {
                                previousPrice = it
                            }
                            flight2?.lowestPrice?.let {
                                nextPrice = it
                            }
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        previousPrice.compareTo(nextPrice)
                    })
            }
            FlightSearchActivity.EARLIEST -> {
                Collections.sort(
                    flightListArr,
                    Comparator<FlightData?> { flight1: FlightData?, flight2: FlightData? ->
                        var previousDepartureTime = 0L
                        var nextDepartureTime = 0L
                        try {
                            flight1?.departureTime?.let {
                                previousDepartureTime = it
                            }
                            flight2?.departureTime?.let {
                                nextDepartureTime = it
                            }
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        previousDepartureTime.compareTo(nextDepartureTime)
                    })
            }
            FlightSearchActivity.FASTEST -> {
                Collections.sort(
                    flightListArr,
                    Comparator<FlightData?> { flight1: FlightData?, flight2: FlightData? ->
                        var previousTime = 0L
                        var nextTime = 0L
                        try {
                            flight1?.difference?.let {
                                previousTime = it
                            }
                            flight2?.difference?.let {
                                nextTime = it
                            }
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        previousTime.compareTo(nextTime)
                    })
            }
        }
        onSort.postValue(flightListArr)
    }
}