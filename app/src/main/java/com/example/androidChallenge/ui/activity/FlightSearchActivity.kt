package com.example.androidChallenge.ui.activity

import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidChallenge.R
import com.example.androidChallenge.data.network.response.FlightData
import com.example.androidChallenge.data.network.response.SortData
import com.example.androidChallenge.databinding.ActivityFlightSeachBinding
import com.example.androidChallenge.databinding.DialogSortLayoutBinding
import com.example.androidChallenge.ui.activity.MainActivity.Companion.DATE
import com.example.androidChallenge.ui.activity.MainActivity.Companion.DESTINATION
import com.example.androidChallenge.ui.activity.MainActivity.Companion.ORIGIN
import com.example.androidChallenge.ui.activity.MainActivity.Companion.TRAVELLER
import com.example.androidChallenge.ui.adapter.FlightListAdapter
import com.example.androidChallenge.ui.modelfactory.FlightSearchViewModelFactory
import com.example.androidChallenge.ui.viewmodel.FlightSearchViewModel
import com.example.androidChallenge.utils.showSnackBar
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_flight_seach.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.ParseException
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList


class FlightSearchActivity : BaseActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory by instance<FlightSearchViewModelFactory>()
    private lateinit var flightSearchViewModel: FlightSearchViewModel
    lateinit var binding: ActivityFlightSeachBinding
    private var flights = ArrayList<FlightData>()
    var origin = ""
    var destination = ""
    var traveller = ""
    var date = ""
    var dialog: BottomSheetDialog? = null
    private var sortRadioGroup: RadioGroup? = null
    private var selectedSortId: Int? = null
    private var sortValuesList = ArrayList<SortData>()
    var radioLargePadding: Int = 0
    var radioSmallPadding: Int = 0
    var radioStandardPadding: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_flight_seach)
        flightSearchViewModel =
            ViewModelProvider(this, factory).get(FlightSearchViewModel::class.java)

        binding.viewModel = flightSearchViewModel

        intent.getStringExtra(ORIGIN)?.let {
            origin = it
        }
        intent.getStringExtra(DESTINATION)?.let {
            destination = "$it."
        }
        intent.getStringExtra(DATE)?.let {
            date = "$it."
        }
        intent.getStringExtra(TRAVELLER)?.let {
            traveller = "$it"
        }

        resources.getInteger(R.integer.radio_large_padding).let {
            radioLargePadding = it
        }

        resources.getInteger(R.integer.radio_small_padding).let {
            radioSmallPadding = it
        }

        resources.getInteger(R.integer.radio_standard_padding).let {
            radioStandardPadding = it
        }

        setToolBar(true, "$origin --> $destination $date $traveller")

        flightSearchViewModel.getFlightSearchResult()

        flightSearchViewModel.onStart.observe(this, Observer {
            progressAnimationView.visibility = View.VISIBLE
        })

        flightSearchViewModel.onSuccess.observe(this, Observer {
            progressAnimationView.visibility = View.GONE
            flights = it as ArrayList<FlightData>
            if (flights.size > 0) {
                errorMessage.visibility = View.GONE
                flightList.visibility = View.VISIBLE
                initRecyclerView()
            } else {
                errorMessage.visibility = View.VISIBLE
                flightList.visibility = View.GONE
            }
        })

        flightSearchViewModel.onFailure.observe(this, Observer {
            root_layout.showSnackBar(it)
            errorMessage.visibility = View.VISIBLE
            errorMessage.text = it
            progressAnimationView.visibility = View.GONE
        })

        sortBy?.setOnClickListener {
            showSortBottomDialog()
        }

        flightSearchViewModel.closeDialogData.observe(this, Observer {
            if (it == CLOSE) {
                dismissDialog()
                dialog?.dismiss()
                dialog = null
            } else if (it == APPLY) {
                for (item in sortValuesList.indices) {
                    if (item == selectedSortId) {
                        sortValuesList[item].isDone = true
                        sortValuesList[item].isSelected = true
                        flightSearchViewModel.sortList(flights, sortValuesList[item].name)
                    } else {
                        sortValuesList[item].isDone = false
                        sortValuesList[item].isSelected = false
                    }
                }
                dialog?.dismiss()
                dialog = null
            }
        })
        sortValuesList.add(SortData(CHEAPEST, isSelected = true, isDone = true))
        sortValuesList.add(SortData(EARLIEST, isSelected = false, isDone = false))
        sortValuesList.add(SortData(FASTEST, isSelected = false, isDone = false))

        flightSearchViewModel.onSort.observe(this, Observer {
            (flightList.adapter as FlightListAdapter).flightList = it
            flightList.adapter?.notifyDataSetChanged()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun initRecyclerView() {
        flightList.layoutManager = LinearLayoutManager(this)
        for (item in flights.indices) {
            val priceList = flights[item].fares?.sortedWith(compareBy { it.fare })
            if (priceList?.size!! > 0) {
                flights[item].lowestFair =
                    resources.getString(R.string.rs) + " ${priceList[0].fare}"
                flights[item].lowestPrice = priceList[0].fare

            }
            var dateDeparture = Date()
            var dateArrival = Date()
            flights[item].departureTime?.let {
                dateDeparture = Date(it)
            }

            flights[item].arrivalTime?.let {
                dateArrival = Date(it)
            }

            val difference: Long = (dateArrival.time) - (dateDeparture.time)
            flights[item].difference = difference
        }
        flightList.adapter = FlightListAdapter(this, flights, flightSearchViewModel)
        flightSearchViewModel.sortList(flights, CHEAPEST)
    }

    private fun showSortBottomDialog() {
        val dialogQualificationBottomSheetBinding = layoutInflater.let {
            DataBindingUtil.inflate<DialogSortLayoutBinding>(
                it,
                R.layout.dialog_sort_layout, null, false
            )
        }.apply {
            viewModel = flightSearchViewModel
            executePendingBindings()
        }

        dialog =
            dialog ?: BottomSheetDialog(this, R.style.TransParentBottomSheetDialog).apply {
                setContentView(dialogQualificationBottomSheetBinding.root)
                setOnDismissListener {
                    dismissDialog()
                }
            }

        dialog?.show()

        showData()
    }

    private fun dismissDialog() {
        for (item in sortValuesList.indices) {
            if (sortValuesList[item].isSelected && !sortValuesList[item].isDone) {
                sortValuesList[item].isSelected = false
            }
        }
    }

    private fun showData() {

        sortRadioGroup = dialog?.findViewById<RadioGroup>(R.id.sortRadioGroup)
        sortRadioGroup?.removeAllViews()

        radioConfiguration(sortValuesList, sortRadioGroup!!)

        selectedSortId?.let {
            if (sortValuesList[selectedSortId!!].isDone && sortValuesList[selectedSortId!!].isSelected)
                sortRadioGroup?.check(it)
        }

        sortRadioGroup?.setOnCheckedChangeListener { radioGroup, i ->

            if (i > -1) {
                val rb: RadioButton? = dialog?.findViewById(i)
                rb?.let {
                    selectedSortId = i
                    val index = sortRadioGroup?.indexOfChild(rb)
                    if (index != null) {
                        for (k in sortValuesList.indices) {
                            if (k == index) {
                                sortValuesList[k].isSelected = true
                            }
                        }
                    }
                }
            }
        }
    }

    private fun radioConfiguration(
        arrList: ArrayList<SortData>,
        radioGroup: RadioGroup
    ) {
        if (!arrList.isNullOrEmpty()) {
            radioGroup.visibility = View.VISIBLE

            for (j in arrList.indices) {

                // Create RadioButton Pragmatically and set Params
                val radioButton = RadioButton(this)
                val params = RadioGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                radioButton.id = j
                params.setMargins(
                    radioSmallPadding,
                    radioSmallPadding,
                    radioSmallPadding,
                    radioSmallPadding
                )
                radioButton.layoutParams = params
                radioButton.setPadding(
                    radioSmallPadding,
                    radioStandardPadding,
                    radioSmallPadding,
                    radioStandardPadding
                )
                radioButton.text = arrList[j].name
                radioButton.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    resources!!.getDimension(R.dimen.sub_title_text)
                )

                if (arrList[j].isSelected) {
                    radioButton.isChecked = true
                    arrList[j].isDone = true
                } else {
                    radioButton.isChecked = false
                }
                radioGroup.addView(radioButton)
            }
        } else {
            radioGroup.visibility = View.GONE
        }
    }

    companion object {
        const val CHEAPEST = "Cheapest"
        const val EARLIEST = "Earliest"
        const val FASTEST = "Fastest"
        const val START = "Start"
        const val CLOSE = "close"
        const val APPLY = "apply"
    }

}
