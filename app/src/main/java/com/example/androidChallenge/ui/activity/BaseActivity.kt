package com.example.androidChallenge.ui.activity

import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.example.androidChallenge.R

open class BaseActivity : AppCompatActivity() {

    var toolbar: Toolbar? = null
    var title: TextView? = null
    var sortBy: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun setToolBar(isShowArrow: Boolean, titleText: String) {
        toolbar = findViewById(R.id.toolbar)
        title = findViewById(R.id.title)
        sortBy = findViewById(R.id.sortBy)
        title?.maxLines = 1
        title?.ellipsize = TextUtils.TruncateAt.END

        title?.text = titleText
        if (isShowArrow) {
            toolbar?.setNavigationIcon(R.drawable.ic_back_arrow)
            sortBy?.visibility = View.VISIBLE
            title?.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        }
        toolbar?.contentInsetStartWithNavigation = 0
        setSupportActionBar(toolbar)

        toolbar?.navigationIcon?.colorFilter =
            BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                ContextCompat.getColor(
                    this,
                    R.color.white
                ), BlendModeCompat.SRC_ATOP
            )
    }

}