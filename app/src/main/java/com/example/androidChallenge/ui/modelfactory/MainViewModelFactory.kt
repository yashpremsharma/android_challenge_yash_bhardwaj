package com.example.androidChallenge.ui.modelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidChallenge.data.repository.FlightSearchRepository
import com.example.androidChallenge.ui.viewmodel.MainViewModel

class MainViewModelFactory() : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel() as T
    }
}