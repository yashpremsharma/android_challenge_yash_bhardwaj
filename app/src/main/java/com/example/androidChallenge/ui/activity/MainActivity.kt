package com.example.androidChallenge.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.androidChallenge.R
import com.example.androidChallenge.databinding.ActivityMainBinding
import com.example.androidChallenge.ui.modelfactory.MainViewModelFactory
import com.example.androidChallenge.ui.viewmodel.MainViewModel
import com.example.androidChallenge.utils.AirPorts
import com.example.androidChallenge.utils.DateFormat
import com.example.androidChallenge.utils.getSimpleDateFormat
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*


class MainActivity : BaseActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory by instance<MainViewModelFactory>()

    private lateinit var journeyDate: String
    private var travellerCount: String = "1 Traveller"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        val mainViewModel =
            ViewModelProvider(this, factory).get(MainViewModel::class.java)

        binding.viewModel = mainViewModel

        setToolBar(false, "Flight")

        val c: Date = Calendar.getInstance().time

        val df = getSimpleDateFormat(DateFormat.USER_READABLE_WITH_TIME)
        df?.let {
            journeyDate = it.format(c)
        }

        binding.journeyDate = journeyDate
        binding.travellers = travellerCount

        mainViewModel.notifyButtonClick.observe(this, androidx.lifecycle.Observer {
            when (it) {

                REFER -> {
                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(
                            Intent.EXTRA_TEXT,
                            resources.getString(R.string.refer_message)
                        )
                        type = "text/plain"
                    }

                    val shareIntent = Intent.createChooser(sendIntent, null)
                    startActivity(shareIntent)
                }

                SEARCH -> {
                    Intent(this, FlightSearchActivity::class.java).also { intent ->
                        intent.putExtra(ORIGIN, AirPorts.DEL.value)
                        intent.putExtra(DESTINATION, AirPorts.BOM.value)
                        if (journeyDate.contains(",")){
                            var date = journeyDate.split(",")
                            if (date.size > 0){
                                intent.putExtra(DATE, date[1])
                            }
                        }
                        intent.putExtra(TRAVELLER, travellerCount)
                        startActivity(intent)
                    }
                }

            }
        })
    }

    companion object {
        const val ORIGIN = "Source"
        const val DESTINATION = "Destination"
        const val DATE = "Date"
        const val TRAVELLER = "Traveller"
        const val REFER = "Refer"
        const val SEARCH = "Search"
    }

}
