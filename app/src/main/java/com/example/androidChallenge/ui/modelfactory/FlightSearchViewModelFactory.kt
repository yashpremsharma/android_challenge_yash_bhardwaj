package com.example.androidChallenge.ui.modelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidChallenge.data.repository.FlightSearchRepository
import com.example.androidChallenge.ui.viewmodel.FlightSearchViewModel
import com.example.androidChallenge.ui.viewmodel.MainViewModel

class FlightSearchViewModelFactory(
    private val repository: FlightSearchRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FlightSearchViewModel(repository) as T
    }
}