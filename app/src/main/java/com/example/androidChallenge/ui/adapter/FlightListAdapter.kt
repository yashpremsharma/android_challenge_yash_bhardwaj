package com.example.androidChallenge.ui.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.DataBindingUtil.inflate
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidChallenge.R
import com.example.androidChallenge.data.network.response.FlightData
import com.example.androidChallenge.databinding.LayoutItemFlightBinding
import com.example.androidChallenge.ui.viewmodel.FlightSearchViewModel
import com.example.androidChallenge.utils.*

class FlightListAdapter(
    private val context: Context,
    var flightList: ArrayList<FlightData>,
    private val model: FlightSearchViewModel
) : RecyclerView.Adapter<FlightListAdapter.TopicNameViewHolder>() {

    private lateinit var layoutInflater: LayoutInflater
    private var departTime: String = ""
    private var arriveTime: String = ""
    private var airLineName: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicNameViewHolder {

        layoutInflater = LayoutInflater.from(parent.context)
        val binding: LayoutItemFlightBinding =
            inflate(layoutInflater, R.layout.layout_item_flight, parent, false)
        return TopicNameViewHolder(binding.root, binding)
    }

    override fun getItemCount() = flightList.size

    override fun onBindViewHolder(holder: TopicNameViewHolder, position: Int) {
        val flightData: FlightData = flightList[position]
        holder.binding.data = flightData
        holder.binding.viewModel = model

        flightData.departureTime?.let {
            departTime = getTimeInHours(it, DateFormat.TWENTY_FOUR_HOURS)
            holder.binding.departureTime = departTime

        }

        flightData.arrivalTime?.let {
            arriveTime = getTimeInHours(it, DateFormat.TWENTY_FOUR_HOURS)
            holder.binding.arrivalTime = arriveTime
        }
        flightData.difference?.let {
            val seconds = it / 1000
            holder.binding.timeDifference = calculateTime(seconds)
        }

        when (flightData.airlineCode) {

            AirLineCode.AIRINDIA.value -> {
                holder.flightIcon.setBackgroundResource(R.drawable.ic_airindia_flight)
                airLineName = "Air India"
                holder.airlineNameText.setTextColor(Color.parseColor("#D8613A"))
            }

            AirLineCode.SPICEJET.value -> {
                holder.flightIcon.setBackgroundResource(R.drawable.ic_spicejet_flight)
                airLineName = "Spice Jet"
                holder.airlineNameText.setTextColor(Color.parseColor("#C60B2A"))
            }

            AirLineCode.GOAIR.value -> {
                holder.flightIcon.setBackgroundResource(R.drawable.ic_goair_flight)
                airLineName = "Go Air"
                holder.airlineNameText.setTextColor(Color.parseColor("#332E80"))
            }

            AirLineCode.JETAIRWAYS.value -> {
                holder.flightIcon.setBackgroundResource(R.drawable.ic_jet_airways_flight)
                airLineName = "Jet Airways"
                holder.airlineNameText.setTextColor(Color.parseColor("#C49E42"))
            }

            AirLineCode.INDIGO.value -> {
                holder.flightIcon.setBackgroundResource(R.drawable.ic_iindigo_flight)
                airLineName = "Indigo"
                holder.airlineNameText.setTextColor(Color.parseColor("#35429F"))
            }
        }
        holder.binding.airlineName = airLineName
        holder.providerList.layoutManager = LinearLayoutManager(context)
        holder.providerList.adapter = ProviderListAdapter(context, flightData.fares!!, model)

    }

    class TopicNameViewHolder(itemView: View, var binding: LayoutItemFlightBinding) :
        RecyclerView.ViewHolder(itemView) {
        var flightIcon: AppCompatImageView = itemView.findViewById(R.id.flightIcon)
        var airlineNameText: AppCompatTextView = itemView.findViewById(R.id.airlineNameText)
        var providerList: RecyclerView = itemView.findViewById(R.id.providerList)
        var priceLayout: LinearLayout = itemView.findViewById(R.id.priceLayout)
    }
}