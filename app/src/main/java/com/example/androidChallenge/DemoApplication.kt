package com.example.androidChallenge

import android.app.Application
import com.example.androidChallenge.data.network.MyApi
import com.example.androidChallenge.data.network.NetworkConnectionInterceptor
import com.example.androidChallenge.data.repository.FlightSearchRepository
import com.example.androidChallenge.ui.modelfactory.FlightSearchViewModelFactory
import com.example.androidChallenge.ui.modelfactory.MainViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class DemoApplication : Application(), KodeinAware{

    override val kodein = Kodein.lazy {
        import(androidXModule(this@DemoApplication))
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from provider { MainViewModelFactory() }
        bind() from singleton { FlightSearchRepository(instance()) }
        bind() from provider { FlightSearchViewModelFactory(instance()) }
    }
}