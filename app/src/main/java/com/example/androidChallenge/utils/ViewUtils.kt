package com.example.androidChallenge.utils

import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

enum class DateFormat {
    USER_READABLE_WITH_TIME, TWENTY_FOUR_HOURS
}

enum class AirLineCode(val value: String) {
    SPICEJET("SG"),
    AIRINDIA("AI"),
    GOAIR("G8"),
    JETAIRWAYS("9W"),
    INDIGO("6E")
}

enum class Providers(val value: Int) {
    MAKEMYTRIP(1),
    CLEARTRIP(2),
    YATRA(3),
    MUSAFIR(4)
}

enum class AirPorts(val value: String) {
    DEL("New Delhi"),
    BOM("Mumbai")
}

fun getSimpleDateFormat(dateFormat: DateFormat): SimpleDateFormat? {
    var simpleDateFormat: SimpleDateFormat? = null
    if (dateFormat == DateFormat.USER_READABLE_WITH_TIME) {
        simpleDateFormat =
            SimpleDateFormat("EEE, dd MMM", Locale.ENGLISH)
    } else if (dateFormat == DateFormat.TWENTY_FOUR_HOURS) {
        simpleDateFormat =
            SimpleDateFormat(
                "HH:mm",
                Locale.ENGLISH
            )
    }
    return simpleDateFormat
}

fun calculateTime(seconds: Long): String {
    val day = TimeUnit.SECONDS.toDays(seconds)
    val hours: Long = TimeUnit.SECONDS.toHours(seconds) -
            TimeUnit.DAYS.toHours(day)
    val minute: Long = TimeUnit.SECONDS.toMinutes(seconds) -
            TimeUnit.DAYS.toMinutes(day) -
            TimeUnit.HOURS.toMinutes(hours)
    val second: Long = TimeUnit.SECONDS.toSeconds(seconds) -
            TimeUnit.DAYS.toSeconds(day) -
            TimeUnit.HOURS.toSeconds(hours) -
            TimeUnit.MINUTES.toSeconds(minute)
    return hours.toString() + "h " + minute.toString() + "m"
}

fun getTimeInHours(time: Long, desiredTimeFormat: DateFormat): String {
    val simpleDateFormat = getSimpleDateFormat(desiredTimeFormat)
    return simpleDateFormat?.format(Date(time)).toString()
}

fun View.showSnackBar(message: String) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG).also { snackbar ->
        snackbar.setAction("OK") {
            snackbar.dismiss()
        }
    }
    (snackbar.view).layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
    (snackbar.view).setPadding(10, 10, 10, 10)
    snackbar.show()
}
