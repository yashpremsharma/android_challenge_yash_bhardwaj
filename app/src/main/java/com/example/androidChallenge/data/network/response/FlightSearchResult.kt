package com.example.androidChallenge.data.network.response

import com.google.gson.annotations.SerializedName

data class FlightSearchResult(
    val flights: ArrayList<FlightData>? = null
)

data class FlightData(
    val originCode: String? = null,
    val destinationCode: String? = null,
    val departureTime: Long? = null,
    val arrivalTime: Long? = null,
    val airlineCode: String? = null,
    @SerializedName("class")
    val classType: String? = null,
    var lowestPrice: Long? = null,
    var lowestFair: String? = null,
    var difference: Long? = null,
    val fares: ArrayList<Fares>? = null
)

data class Fares(
    val providerId: Int? = null,
    val fare: Long? = null
)