package com.example.androidChallenge.data.network.response

data class SortData(
    val name: String,
    var isSelected: Boolean,
    var isDone: Boolean
)