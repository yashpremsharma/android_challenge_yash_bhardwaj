package com.example.androidChallenge.data.repository

import com.example.androidChallenge.data.network.MyApi
import com.example.androidChallenge.data.network.SafeApiRequest
import com.example.androidChallenge.data.network.response.FlightSearchResult

class FlightSearchRepository(
    private val api: MyApi
): SafeApiRequest() {

    suspend fun getFlightSearchResult(): FlightSearchResult {
        return apiRequest { api.getFlightList() }
    }
}